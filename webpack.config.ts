import path from 'path';
import webpack from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin'
// in case you run into any typescript error when configuring `devServer`
import 'webpack-dev-server';
import { buildWebpack } from './config/build/buildWebpack';

type Mode = 'development' | 'production'

interface envVariables {
  mode : Mode
  port: number
}

export default (env:envVariables) => {
  const config: webpack.Configuration = buildWebpack({
    mode: env.mode ?? "development",
    paths: {
      entry: path.resolve(__dirname, 'src', 'index.tsx'),
      html: path.resolve(__dirname, 'public', 'index.html'),
      output: path.resolve(__dirname, 'build'),
    },
    port: env.port ?? 3000
  })
  return config
}