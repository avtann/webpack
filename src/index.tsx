import { createRoot } from 'react-dom/client'
import { App } from './components/App'
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import { LazyAbout } from './pages';
import { LazyShop } from './pages/shop/ShopLazy';
import { Suspense } from 'react';


const router = createBrowserRouter([
  {
    path: "/",
    element: <App/>,
    children: [
      {
        path: "about",
        element:<LazyAbout/> 
      },
      {
        path: "shop",
        element: <Suspense fallback={'Loading...'}>
        <LazyShop/>
      </Suspense>,
      },
    ]
  },
]);

const root = document.getElementById('root')

if(!root){
  throw new Error('root not found')
}

const container = createRoot(root)

container.render(
  <RouterProvider router={router}/>
)

