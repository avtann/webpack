import { useState } from 'react'
import s from './app.modules.css'
import { Outlet } from 'react-router-dom'


export function App() {
  const [count, setCount] = useState(0)
  return (
    <div className={s.app} onClick={() => setCount(v => v+1)} onDoubleClick={() => setCount(v => v-2)}>{count}
    <Outlet/></div>
  )
}
